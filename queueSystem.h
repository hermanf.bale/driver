#pragma once

void initQue(void);

void addToQueue(int b, int f);
void removeFromQueue(int b, int f);
void updateQueue(void);
void checkQueue(void);