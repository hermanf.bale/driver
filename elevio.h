/**
 * @file elevio.h
 * @brief Initialiserer enums og funksjoner som er utdelt - henter informasjon fra den fysiske heisen.
 */

#pragma once


#define N_FLOORS 4

typedef enum { 
    DIRN_DOWN   = -1,
    DIRN_STOP   = 0,
    DIRN_UP     = 1
} MotorDirection;


#define N_BUTTONS 3

typedef enum { 
    BUTTON_HALL_UP      = 0,
    BUTTON_HALL_DOWN    = 1,
    BUTTON_CAB          = 2
} ButtonType;

/**
 * @brief Initialiserer den fysiske heisen.
 */
void elevio_init(void);

/**
 * @brief Setter motorpådrag på fysisk heis.
 * 
 * @param dirn Retiningen heisen skal gå.
 */
void elevio_motorDirection(MotorDirection dirn);
/**
 * @brief Slår på lyset til knappen som tilsvarer parameterne.
 * @param floor Etasjen
 * @param button Type knapp
 * @param value Bestemmer om lys skal av eller på.
 */
void elevio_buttonLamp(int floor, ButtonType button, int value);
/**
 * @brief Setter lyset til etasjeindikatoren ved aktuell etasje.
 * @param floor Etasjen
 */
void elevio_floorIndicator(int floor);
/**
 * @brief "Åpner døren" - Setter lyset som indikerer om døren er åpen eller ikke.
 * @param value Bestemmer om lys skal av eller på.
 */
void elevio_doorOpenLamp(int value);
/**
 * @brief Setter lyset til stoppknappen.
 * @param value Bestemmer om lys skal av eller på.
 */
void elevio_stopLamp(int value);

/**
 * @brief Returnerer hvilken etasje som heisen er tilkalt til.
 * 
 * @param floor Etasjen
 * @param button Type knapp
 * @return Etasjen
 */
int elevio_callButton(int floor, ButtonType button);
/**
 * @brief Indikerer hvorvidt heisen er ved en etasje.
 * 
 * @return int
 */
int elevio_floorSensor(void);
/**
 * @brief Indikerer om stopknappen er trykket.
 * 
 * @return int 
 */
int elevio_stopButton(void);
/**
 * @brief Indikerer om det finnes en obstruksjon.
 * 
 * @return int 
 */
int elevio_obstruction(void);
