#include "elevio.h"
#include "queueSystem.h"

int g_queue[N_BUTTONS][N_FLOORS];
int *(p_queue)=&g_queue;

void initQueue(){
    for(int b=0;b<N_BUTTONS;b++){
        for(int f=0;f<N_FLOORS;f++){
            *p_queue[b][f]=0;
        }
    }
}

void addToQueue(int b, int f){
    int addFloor=-1;
    int *p_addFloor=&addFloor;

    *p_addFloor = elevio_callButton(f,b);
    if(addFloor!=-1){
        *p_queue[b][f]=1;
        elevio_buttonLamp(b,f,*p_addFloor);
    }
    *p_addFloor = -1;   
}

void removeFromQueue(int b, int f){
    if(state == OPEN){
        *p_queue[b][f] = 0;
    }
}

void updateQueue(){
    for(int b=0;b<N_BUTTONS;b++){
        for(int f=0;f<N_FLOORS;f++){
            addToQueue(b,f);
            removeFromQueue(b,f);
        }
    }
}


void checkQueue(){
    static int prioOne = 2;
    for(int f=0;f<N_FLOORS;f++){
        if(*p_queue[prioOne][f]==1){
            if(nextFloor == -1){
                nextFloor == elevio_callButton(f,prioOne);
            }
            if(currentFloor<nextFloor && state == UP){
                nextFloor == elevio_callButton(f,prioOne);
            }
            if(currentFloor>nextFloor && state == DOWN){
                nextFloor == elevio_callButton(f,prioOne);
            }
        }
        else{continue;}
    }
    for
}
