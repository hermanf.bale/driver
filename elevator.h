#pragma once

#include "elevio.h"

int g_currentFloor = elevio_floorSensor();

typedef enum{
    STAT    = 0,
    OPEN    = 1,
    DOWN    = 2,
    UP      = 3
} STATE;